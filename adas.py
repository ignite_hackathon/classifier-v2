
@routes.post('/get')
async def hello(request):
    body = await request.json()
    file = open('./categories.json')
    movements = body['movements']

    categories = json.load(file)

    categorized_list = []


    for cat in categories:
        if 'keywords' in cat:
            ids = get_matching_ids(body['movements'], cat['keywords'])
            for move in movements:
                if move['id'] in ids:
                    move.update({'categories':  [cat['name']]})
                    categorized_list = update_categorized_list(move, categorized_list)

        if 'leaves' in cat:
            for l in cat['leaves']:
                ids = get_matching_ids(body['movements'], l['keywords'])
                for move in movements:
                    if move['id'] in ids:
                        move.update({'categories':  [l['name']]})
                        categorized_list = update_categorized_list(move, categorized_list)


    body = {'moves': categorized_list}

    return web.json_response(body)



def get_matching_ids(moves, keywords) -> list:
    keywords = [k.upper() for k in keywords]
    res = Resolver(json.dumps(moves), json.dumps({'words': keywords}))
    resolved = res.resolve()
    ids = [m['id'] for m in resolved]
    return ids


def update_categorized_list(move, categorized):
    ret = categorized
    if len(ret) == 0:
        ret.append(move)
        return categorized

    for i, m in enumerate(categorized):
        if m['id'] == move['id']:
            ret[i]['categories'].append(move['categories'])
        else:
            ret.append(move)

    return ret

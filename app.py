from aiohttp import web
from resolver import Resolver


import json
routes = web.RouteTableDef()

# Health check
@routes.get('/ping')
async def health(request):
    return web.Response(text="OK")

@routes.get('/')
async def hello(request):
    return web.Response(text="Hello, world")


def get_categories_from_moves(categorized):
    ret = [ ]
    for move in categorized:
        cates = move['categories']
        ret += cates

    return set(ret)


@routes.post('/get')
async def hello(request):
    body = await request.json()
    file = open('./categories.json')
    movements = body['movements']

    categories = json.load(file)

    categorized = []

    for cat in categories:

        if 'keywords' in cat:
            keywords = [k.upper() for k in cat['keywords']]

            res = Resolver(json.dumps(body['movements']), json.dumps({'words': keywords}))
            response = res.resolve()

            ids = [m['id'] for m in response]
            found = False

            for move in movements:
                if move['id'] in ids:
                    for categorized_move in categorized:
                        if move['id'] == categorized_move['id']:
                            found = True
                            move_categories = categorized_move['categories']
                            move_categories.append(cat['name'])
                            categorized_move.update({'categories': move_categories})
                            break

                    if not found:
                        new_move = move.copy()
                        new_move.update({'categories': [cat['name']]})
                        categorized.append(new_move)


        if 'leaves' in cat:
            for leave in cat['leaves']:
                keywords = [k.upper() for k in leave['keywords']]

                res = Resolver(json.dumps(body['movements']), json.dumps({'words': keywords}))
                response = res.resolve()

                ids = [m['id'] for m in response]

                found = False

                for move in movements:
                    if move['id'] in ids:
                        for categorized_move in categorized:
                            if move['id'] == categorized_move['id']:
                                found = True
                                move_categories = categorized_move['categories']
                                move_categories.append(cat['name'])
                                move_categories.append(leave['name'])
                                categorized_move.update({'categories': move_categories})
                                break

                        if not found:
                            new_move = move.copy()
                            new_move.update({'categories': [cat['name'], leave['name']]})
                            categorized.append(new_move)

    categories = get_categories_from_moves(categorized)
    categories_list = list(categories)

    categories_list = ', '.join(categories_list)

    body = { 'moves': categorized, 'categories': categories_list }

    return web.json_response(body)

import nltk
nltk.download('punkt')
nltk.download('stopwords')
print('download')

app = web.Application()
app.add_routes(routes)
web.run_app(app)
